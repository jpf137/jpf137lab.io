---
layout: page
title: About
permalink: /about/
---

<div markdown="1">
![](/hudf2.jpg){:width="360px"}![](/cells2jpg.jpg){:width="360px"}
![](/code.jpg ){:width="360px"}![](/earth3.jpg ){:width="360px"}
</div>




This is the base Jekyll theme. You can find out more info about customizing your Jekyll theme, as well as basic Jekyll usage documentation at [jekyllrb.com](http://jekyllrb.com/)

You can find the source code for the Jekyll new theme at:
{% include icon-github.html username="jglovier" %} /
[jekyll-new](https://github.com/jglovier/jekyll-new)

You can find the source code for Jekyll at
{% include icon-github.html username="jekyll" %} /
[jekyll](https://github.com/jekyll/jekyll)

